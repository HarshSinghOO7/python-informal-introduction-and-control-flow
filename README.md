# Python Informal Introduction and Control Flow

### Numbers

There are only three in-build data types present in python **int, float and complex**. We can perform different action using the arithmetic operator present in python. There are total 7 arithmetic operation present in python addition, subtraction, multiplication, division, modulus, exponentiation, floor. Here are some arithmetic operations. 

```python
2 + 2 
```

```python
3 * 5 # this is the simple multiplication operation
```

```python
10 % 5 # the output of this file 
```

```python
2 ** 3 # the output of this operation is 2 power 3.
```

```python
3 / 5 # it will give value in float
```

```python
10 // 7 #it is a floor division will give value in int 
```

**Note:- The two division operation above have different results. If we use a single (/) division sign the output value comes float but if we use double(//) division sign then it floors the value and output comes in int.**

```python
2 + 3j # it a complex number
```

We can use (=) sign to assign the value to a variable to perform some actions.

```python
a = 5
b = 10
a + b
```

### Strings

String in python are immutable means there values cannot be changed once it got assigned. Strings are assigned either by ('')single quote or ("") with double quotes.

```python
'Harsh'
```

```python
"Harsh"
```

**Note:- We can use in-build python function type() to verify the types of both the string.**

If your string contains words like doesn't, I'll, etc. using double quote("") are most like to be preferred but if you want to use single quote then use backslash(\) in the place where you'll add a single quote. Eg:-

```python
'doesn\'t' # use \' to escape the single quote
```

But there are some senarios where \ with some characters have some means in python **Eg:- \n**. So using r before the single quote is more likely to be used.

```python
print(r'C:\some\name')  # note the r before the quote
```

If you want some special syntax to be maintained in the string than you can use triple quotes (''') or (""") in the starting and in the end of the strings.

```python
print("""
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")
```

String concatenation can only be performed using the (+) symbol and repeatation can be done by (*). The proper syntax for using the symbols is:

```python
3 * 'un' + 'ium'
```

```python
name = "Har"
name + "sh"
```

String can be indexed and get accessed using the index number. The first index starts from 0. There is no character data type in python. A character datatype here can be a string of size one.

```python
word = "Harsh"
word[0]  # character at position 0.
```

We can also excess the values from the right hand side using negative values and the index for accessing it start from -1.

```python
word = "Iron Man"
word[-1] # character at position last.
```

**Note:- If we try to access the index more than the length of your string than it will give --string index out of range error-- **


**Slicing** can also be perform on the string to access a string in a particular range. The value that you'll give after semicolon(:) will get exclude and the value before that number will be taken.

```python
word = "Monday"
word[0:2]  # characters from position 0 (included) to 2 (excluded)
```

```python
word = 'Vision'
word[4:] # characters from position 4 (included) to the end
```

You can perform concatenation operation using the slicing to make a new string.

```python
name = "Python"
'S' + name[1:]
```

There is an in-build function **len()** to find the length of the string.

```python
string = "Dikshant"
print(len(string)) 
```

### Lists

List is a type of array where we can store collection of data in a single variable. It can contains values of different data types. List are initalise using ([]) square brackets or list(). And the accessing of the values is same as in the strings. We can access the values using the indexes or from slicing.

```python
squares = [1, 4, 9, 16, 25]
squares
```

```python
squares[0]  # indexing returns the item
```

```python
squares[-3:]  # slicing returns a new list
```

We can also perform operations like concatenations where we can concatenate 2 lists and combine it into one.

```python
squares + [36, 49, 64, 81, 100]
```

- Methods in list 
     - append() # to add items in the list.
     - len() # to find the length of the list.
     - insert() # to insert an element at a specific index in the list.
     - extend() # to append elements from another list

# Control flow in python

### If Statement

If is a condition statement judge on the bases on true or false. There can be multiple if and else statement in the files. For more than 2 condition in a statement we use **elif**.

```python
x = 43
if x < 0:
    x = 0
    print('Negative changed to zero')
elif x == 0:
    print('Zero')
elif x == 1:
    print('Single')
else:
    print('More')
```

### For Statement

For statement are used for iterating over a list, array, string, etc.

```python
words = ['shivam', 'ritesh', 'manish']
for w in words:
    print(w)
```

```python
users = {'Hans': 'active', 'Éléonore': 'inactive', '景太郎': 'active'}
for user in users.copy().items():
    print(user)
```

### Range

Range function is used to make a sequence of a number starting from 0. It has 3 parameter in which we can specify the starting number, ending number, sequence of a number (range(start, stop, step)).

```python
for i in range(5):
    print(i)
```

```python
for i in range(1,5):
    print(i)
```

```python
for i in range(1,10,2):
    print(i)
```

We can combine range() and len() function to iterate in arrays, or in list.

```python
a = ['Mudassir', 'Krishna', 'Manish', 'Dikshant', 'Shivam',1,2]
for i in range(len(a)):
    print(a[i])
```

### Break, Continue and Else Clause

Break is a loop control statement used to terminate the function or a loop when break is encountered.

```python
for i in range(10):
    if i == 7:
        break
    print(i)
```

Continue is used to end the current iteration and go to the next one. Basically if a countinue keyword is encountered the statement before that will not be executed.

```python
for i in range(10):
    if i % 3 == 0:
        continue
    print(i)
```

Else clause is used when the **for loop** gets terminated by a break statement.

```python
for i in range(1,10):
    if i % 4 == 0:
        break
    print(i)
else:
    print("This is an else statement encountered after break")
```

### Pass

Pass statement does nothing. It is a kind a placeholder in a function that there might can be some code come in a function.

```python
def fun():
    pass
fun()
```

### Match Statement

Match statement is a type of switch statement used in C, C++, Java, etc. It used to evaluate through multiple condition at a same time it has 

```python
def http_error(status):
    match status:
        case 400:
            return "Bad request"
        case 404:
            return "Not found"
        case 418:
            return "I'm a teapot"
        case _:
            return "Something's wrong with the internet"
```
The last case contain '_' wildcard just like default in switch. If neither of the cases matches then the default case get implemented.

You can also combine multiple case using (|) or symbol

```python
def http_error(status):
    match status:
        case 400 | 404 | 418:
            return "Issue is from our end"
        case _:
            return "Something's wrong with the internet"
```

### Defining Function

We can use **def** keyword to define a function name. def functionname() is used to make a function. We can create n number of function using the **def** keyword.

```python
def fib(n):    # write Fibonacci series up to n
    """Print a Fibonacci series up to n."""
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()
```

- **Default Arguments**:
  Default arguments are those arguments in the function which are called by default if no parameters are passed. It is the most useful form to specifiy a default value.
  ```python
  def ans(a, b = 10): # these are the default arguments.
    return a + b

  ```

- **Keyword Argument**:
  Keyword argument are those arguments in which while passing the value we specify that the variable name same as in function. To let function know that the variable we are specifying is for a particular argument of the same name.
  ```python
  ans(b = 20,a = 10)
  ```
  There are some othe form of keyword arguments.
  ```python
  parrot(1000)                                          # 1 positional argument
  parrot(voltage=1000)                                  # 1 keyword argument
  parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
  parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
  parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
  parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword
  ```

### Lambda Expression

Lambda keyword is used to create a small anonymous function. The lambda function is used for nameless function to perform some desired actions like adding numbers from and array or iterating within an array.

```python
def add(n):
    return lambda x: x + n
```

Lambda function can be also useful for sorting a **dict** with it's keys.

```python
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[1])
pairs
```

### Documentation String

Documentation string are those string which we write in a function to let other people know about what the function does and what action it performs used within (''') or ("""). The first line should be a consise summary of what are the objects and second line should be blank for indentation and other line describing about the functions.

Examples of documentation String are:

```python
def my_function():
    """Do nothing, but document it.

    No, really, it doesn't do anything.
    """
    pass
```
